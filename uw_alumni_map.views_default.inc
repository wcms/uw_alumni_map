<?php

/**
 * @file
 * uw_alumni_map.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_alumni_map_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'alumni_world_map';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Alumni World Map';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Alumni World Map';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
    4 => '4',
    5 => '5',
    6 => '6',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '25';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Header: Map */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['ui_name'] = 'Map';
  $handler->display->display_options['header']['area']['label'] = 'Map';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = '<img src="/fdsu5/sites/d7.fdsu5/modules/uw_alumni_map/images/world_map.jpg" width="500" height="321" border="0" alt="Map of the world, separated into regions" usemap="#imap" />
<map name="imap" id="imap">
	<area shape="poly" coords="66,57,33,66,17,89,25,124,58,129,80,152,120,169,135,168,143,174,148,173,144,164,184,137,184,104,237,63,237,24,201,17,132,17"  href="/fdsu5/alumni-affairs/where-are-waterloo-alumni/north-america" alt="North America" />
	<area shape="poly" coords="477,79,368,37,320,66,315,77,322,92,320,101,311,128,298,127,293,131,303,146,311,146,333,154,324,175,324,214,363,280,479,280" href="/fdsu5/alumni-affairs/where-are-waterloo-alumni/asia-pacific" alt="Asia & Pacific" />
	<area shape="poly" coords="254,264,316,238,314,208,295,208,302,191,290,191,275,162,258,158,244,153,231,153,221,156,205,176,205,191,218,205,235,209"  href="/fdsu5/alumni-affairs/where-are-waterloo-alumni/africa" alt="Africa" />
	<area shape="poly" coords="159,302,175,292,168,277,209,219,203,208,167,189,155,188,136,203,135,232,147,239,144,288,146,288" href="/fdsu5/alumni-affairs/where-are-waterloo-alumni/south-america" alt="South America" />
	<area shape="poly" coords="290,190,323,175,334,155,313,146,293,145,287,142,268,146,266,150,267,154,276,158,277,163" href="/fdsu5/alumni-affairs/where-are-waterloo-alumni/middle-east" alt="Middle East" />
	<area shape="poly" coords="303,65,266,71,216,82,209,86,217,132,216,155,223,154,230,154,246,151,253,156,266,153,270,144,283,141,288,142,296,144,295,129,312,128,321,94,314,79" href="/fdsu5/alumni-affairs/where-are-waterloo-alumni/europe" alt="Europe" />
	<area shape="poly" coords="106,190,131,200,150,186,184,189,201,166,155,160,144,175,136,173,119,169,85,155" href="/fdsu5/alumni-affairs/where-are-waterloo-alumni/central-america-caribbean" alt="Central America & Caribbean" />
</map>';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['content'] = 'University of Waterloo alumni live and work throughout the world. Click on the regions in the map above to see the number of alumni in each country.';
  $handler->display->display_options['empty']['area']['format'] = 'uw_tf_standard';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h3';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Region (field_region) */
  $handler->display->display_options['arguments']['field_region_value']['id'] = 'field_region_value';
  $handler->display->display_options['arguments']['field_region_value']['table'] = 'field_data_field_region';
  $handler->display->display_options['arguments']['field_region_value']['field'] = 'field_region_value';
  $handler->display->display_options['arguments']['field_region_value']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_region_value']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_region_value']['default_argument_options']['argument'] = 'Default';
  $handler->display->display_options['arguments']['field_region_value']['default_argument_skip_url'] = TRUE;
  $handler->display->display_options['arguments']['field_region_value']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_region_value']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_region_value']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator_id'] = '';
  $handler->display->display_options['filters']['status']['expose']['label'] = 'Published';
  $handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
  $handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';
  $handler->display->display_options['filters']['status']['expose']['required'] = TRUE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'alumni_world_map_data' => 'alumni_world_map_data',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'alumni_world_map');
  $handler->display->display_options['defaults']['access'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['path'] = 'alumni-affairs/where-are-waterloo-alumni';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Where are Waterloo alumni?';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;

  /* Display: Manage alumni world map data */
  $handler = $view->new_display('page', 'Manage alumni world map data', 'manage_alumni_world_map_data');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Create/Manage Alumni World Map Data';
  $handler->display->display_options['defaults']['use_ajax'] = FALSE;
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '25';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['expose']['items_per_page'] = TRUE;
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options'] = '10, 25, 50, 100, 200';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'nid' => 'nid',
    'title' => 'title',
    'field_region' => 'field_region',
    'changed' => 'changed',
    'status' => 'status',
    'edit_node' => 'edit_node',
    'delete_node' => 'delete_node',
    'nothing' => 'nothing',
    'nothing_2' => 'nothing_2',
    'nothing_1' => 'nothing_1',
  );
  $handler->display->display_options['style_options']['default'] = 'title';
  $handler->display->display_options['style_options']['info'] = array(
    'nid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_region' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'changed' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'status' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'edit_node' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'delete_node' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing_2' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing_1' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['label'] = 'Alumni World Map Data Description';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = '<p>Information to feed the alumni world map.</p>';
  $handler->display->display_options['header']['area']['format'] = 'uw_tf_standard';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area_1']['id'] = 'area_1';
  $handler->display->display_options['header']['area_1']['table'] = 'views';
  $handler->display->display_options['header']['area_1']['field'] = 'area';
  $handler->display->display_options['header']['area_1']['label'] = 'Action Links';
  $handler->display->display_options['header']['area_1']['empty'] = TRUE;
  $handler->display->display_options['header']['area_1']['content'] = '<ul class="action-links">
<li>
<a href="../../../node/add/uw-alumni-world-map-data">Add alumni world map data</a>
</li>
</ul>';
  $handler->display->display_options['header']['area_1']['format'] = 'uw_tf_standard';
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'No alumni world map data text';
  $handler->display->display_options['empty']['area']['content'] = '<em>No alumni world map data has been found matching your criteria. To add alumni world map data, please use the link above.</em>';
  $handler->display->display_options['empty']['area']['format'] = 'uw_tf_standard';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  /* Field: Content: Region */
  $handler->display->display_options['fields']['field_region']['id'] = 'field_region';
  $handler->display->display_options['fields']['field_region']['table'] = 'field_data_field_region';
  $handler->display->display_options['fields']['field_region']['field'] = 'field_region';
  /* Field: Content: Updated date */
  $handler->display->display_options['fields']['changed']['id'] = 'changed';
  $handler->display->display_options['fields']['changed']['table'] = 'node';
  $handler->display->display_options['fields']['changed']['field'] = 'changed';
  $handler->display->display_options['fields']['changed']['label'] = 'Last updated';
  $handler->display->display_options['fields']['changed']['date_format'] = 'time ago';
  /* Field: Content: Published */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'node';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  $handler->display->display_options['fields']['status']['not'] = 0;
  /* Field: Content: Edit link */
  $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['exclude'] = TRUE;
  $handler->display->display_options['fields']['edit_node']['text'] = 'Edit';
  /* Field: Content: Delete link */
  $handler->display->display_options['fields']['delete_node']['id'] = 'delete_node';
  $handler->display->display_options['fields']['delete_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['delete_node']['field'] = 'delete_node';
  $handler->display->display_options['fields']['delete_node']['exclude'] = TRUE;
  $handler->display->display_options['fields']['delete_node']['text'] = 'Delete';
  /* Field: View link */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['ui_name'] = 'View link';
  $handler->display->display_options['fields']['nothing']['label'] = 'View link';
  $handler->display->display_options['fields']['nothing']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nothing']['alter']['text'] = 'View';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing']['alter']['path'] = 'node/[nid]';
  /* Field: Clone link */
  $handler->display->display_options['fields']['nothing_2']['id'] = 'nothing_2';
  $handler->display->display_options['fields']['nothing_2']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_2']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_2']['ui_name'] = 'Clone link';
  $handler->display->display_options['fields']['nothing_2']['label'] = 'Clone link';
  $handler->display->display_options['fields']['nothing_2']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nothing_2']['alter']['text'] = 'Clone';
  $handler->display->display_options['fields']['nothing_2']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing_2']['alter']['path'] = 'node/[nid]/clone';
  /* Field: Actions */
  $handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
  $handler->display->display_options['fields']['nothing_1']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_1']['ui_name'] = 'Actions';
  $handler->display->display_options['fields']['nothing_1']['label'] = 'Actions';
  $handler->display->display_options['fields']['nothing_1']['alter']['text'] = '<ul class="links manage-actions">
  <li>[nothing]</li>
  <li>[edit_node]</li>
  <li>[nothing_2]</li>
  <li>[delete_node]</li>
</ul>';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'alumni_world_map_data' => 'alumni_world_map_data',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Title */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'node';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['operator'] = 'contains';
  $handler->display->display_options['filters']['title']['group'] = 1;
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['label'] = 'Title';
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
  /* Filter criterion: Content: Region (field_region) */
  $handler->display->display_options['filters']['field_region_value']['id'] = 'field_region_value';
  $handler->display->display_options['filters']['field_region_value']['table'] = 'field_data_field_region';
  $handler->display->display_options['filters']['field_region_value']['field'] = 'field_region_value';
  $handler->display->display_options['filters']['field_region_value']['group'] = 1;
  $handler->display->display_options['filters']['field_region_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_region_value']['expose']['operator_id'] = 'field_region_value_op';
  $handler->display->display_options['filters']['field_region_value']['expose']['label'] = 'Region';
  $handler->display->display_options['filters']['field_region_value']['expose']['operator'] = 'field_region_value_op';
  $handler->display->display_options['filters']['field_region_value']['expose']['identifier'] = 'field_region_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['exposed'] = TRUE;
  $handler->display->display_options['filters']['status']['expose']['operator_id'] = '';
  $handler->display->display_options['filters']['status']['expose']['label'] = 'Published';
  $handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
  $handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';
  $handler->display->display_options['filters']['status']['expose']['required'] = TRUE;
  $handler->display->display_options['path'] = 'admin/workbench/create/alumni-world-map-data';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Alumni world map data';
  $handler->display->display_options['menu']['weight'] = '51';
  $handler->display->display_options['menu']['context'] = 0;
  $translatables['alumni_world_map'] = array(
    t('Master'),
    t('Alumni World Map'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Map'),
    t('<img src="/fdsu5/sites/d7.fdsu5/modules/uw_alumni_map/images/world_map.jpg" width="500" height="321" border="0" alt="Map of the world, separated into regions" usemap="#imap" />
<map name="imap" id="imap">
	<area shape="poly" coords="66,57,33,66,17,89,25,124,58,129,80,152,120,169,135,168,143,174,148,173,144,164,184,137,184,104,237,63,237,24,201,17,132,17"  href="/fdsu5/alumni-affairs/where-are-waterloo-alumni/north-america" alt="North America" />
	<area shape="poly" coords="477,79,368,37,320,66,315,77,322,92,320,101,311,128,298,127,293,131,303,146,311,146,333,154,324,175,324,214,363,280,479,280" href="/fdsu5/alumni-affairs/where-are-waterloo-alumni/asia-pacific" alt="Asia & Pacific" />
	<area shape="poly" coords="254,264,316,238,314,208,295,208,302,191,290,191,275,162,258,158,244,153,231,153,221,156,205,176,205,191,218,205,235,209"  href="/fdsu5/alumni-affairs/where-are-waterloo-alumni/africa" alt="Africa" />
	<area shape="poly" coords="159,302,175,292,168,277,209,219,203,208,167,189,155,188,136,203,135,232,147,239,144,288,146,288" href="/fdsu5/alumni-affairs/where-are-waterloo-alumni/south-america" alt="South America" />
	<area shape="poly" coords="290,190,323,175,334,155,313,146,293,145,287,142,268,146,266,150,267,154,276,158,277,163" href="/fdsu5/alumni-affairs/where-are-waterloo-alumni/middle-east" alt="Middle East" />
	<area shape="poly" coords="303,65,266,71,216,82,209,86,217,132,216,155,223,154,230,154,246,151,253,156,266,153,270,144,283,141,288,142,296,144,295,129,312,128,321,94,314,79" href="/fdsu5/alumni-affairs/where-are-waterloo-alumni/europe" alt="Europe" />
	<area shape="poly" coords="106,190,131,200,150,186,184,189,201,166,155,160,144,175,136,173,119,169,85,155" href="/fdsu5/alumni-affairs/where-are-waterloo-alumni/central-america-caribbean" alt="Central America & Caribbean" />
</map>'),
    t('University of Waterloo alumni live and work throughout the world. Click on the regions in the map above to see the number of alumni in each country.'),
    t('All'),
    t('Published'),
    t('Page'),
    t('Manage alumni world map data'),
    t('Create/Manage Alumni World Map Data'),
    t('Alumni World Map Data Description'),
    t('<p>Information to feed the alumni world map.</p>'),
    t('Action Links'),
    t('<ul class="action-links">
<li>
<a href="../../../node/add/uw-alumni-world-map-data">Add alumni world map data</a>
</li>
</ul>'),
    t('No alumni world map data text'),
    t('<em>No alumni world map data has been found matching your criteria. To add alumni world map data, please use the link above.</em>'),
    t('Nid'),
    t('Title'),
    t('Region'),
    t('Last updated'),
    t('Edit link'),
    t('Edit'),
    t('Delete link'),
    t('Delete'),
    t('View link'),
    t('View'),
    t('Clone link'),
    t('Clone'),
    t('Actions'),
    t('<ul class="links manage-actions">
  <li>[nothing]</li>
  <li>[edit_node]</li>
  <li>[nothing_2]</li>
  <li>[delete_node]</li>
</ul>'),
  );
  $export['alumni_world_map'] = $view;

  return $export;
}
