<?php

/**
 * @file
 * uw_alumni_map.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function uw_alumni_map_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_region'.
  $field_bases['field_region'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_region',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'default' => 'Default',
        'africa' => 'Africa',
        'asia-pacific' => 'Asia & Pacific',
        'central-america-caribbean' => 'Central America & Caribbean',
        'europe' => 'Europe',
        'middle-east' => 'Middle East',
        'north-america' => 'North America',
        'south-america' => 'South America',
      ),
      'allowed_values_function' => '',
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
