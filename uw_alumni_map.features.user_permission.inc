<?php

/**
 * @file
 * uw_alumni_map.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_alumni_map_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create alumni_world_map_data content'.
  $permissions['create alumni_world_map_data content'] = array(
    'name' => 'create alumni_world_map_data content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any alumni_world_map_data content'.
  $permissions['delete any alumni_world_map_data content'] = array(
    'name' => 'delete any alumni_world_map_data content',
    'roles' => array(
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own alumni_world_map_data content'.
  $permissions['delete own alumni_world_map_data content'] = array(
    'name' => 'delete own alumni_world_map_data content',
    'roles' => array(
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any alumni_world_map_data content'.
  $permissions['edit any alumni_world_map_data content'] = array(
    'name' => 'edit any alumni_world_map_data content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own alumni_world_map_data content'.
  $permissions['edit own alumni_world_map_data content'] = array(
    'name' => 'edit own alumni_world_map_data content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  return $permissions;
}
