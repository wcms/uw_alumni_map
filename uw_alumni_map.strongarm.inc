<?php

/**
 * @file
 * uw_alumni_map.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_alumni_map_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_alumni_world_map_data';
  $strongarm->value = array();
  $export['menu_options_alumni_world_map_data'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_alumni_world_map_data';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_alumni_world_map_data'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_alumni_world_map_data';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );
  $export['node_options_alumni_world_map_data'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_alumni_world_map_data';
  $strongarm->value = '0';
  $export['node_preview_alumni_world_map_data'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_alumni_world_map_data';
  $strongarm->value = 0;
  $export['node_submitted_alumni_world_map_data'] = $strongarm;

  return $export;
}
