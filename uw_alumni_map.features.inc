<?php

/**
 * @file
 * uw_alumni_map.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_alumni_map_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_alumni_map_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_alumni_map_node_info() {
  $items = array(
    'alumni_world_map_data' => array(
      'name' => t('Alumni world map data'),
      'base' => 'node_content',
      'description' => t('Information to feed the alumni world map.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
